import itertools
import numpy as np

def affine_forward(x, w, b):
  """
  Computes the forward pass for an affine (fully-connected) layer.

  The input x has shape (N, d_1, ..., d_k) where x[i] is the ith input.
  We multiply this against a weight matrix of shape (D, M) where
  D = \prod_i d_i

  Inputs:
  x - Input data, of shape (N, d_1, ..., d_k)
  w - Weights, of shape (D, M)
  b - Biases, of shape (M,)

  Returns a tuple of:
  - out: output, of shape (N, M)
  - cache: (x, w, b)
  """
  N = x.shape[0]
  # Need to convert (N, d_1, ..., d_k) into (N, D),
  # reshape will concatenate the elements along the last k dimensions
  # row wise
  out = x.reshape(N, -1).dot(w) + b
  cache = (x, w, b)
  return out, cache


def affine_backward(dout, cache):
  """
  Computes the backward pass for an affine layer.

  Inputs:
  - dout: Upstream derivative, of shape (N, M)
  - cache: Tuple of:
    - x: Input data, of shape (N, d_1, ... d_k)
    - w: Weights, of shape (D, M)

  Returns a tuple of:
  - dx: Gradient with respect to x, of shape (N, d1, ..., d_k)
  - dw: Gradient with respect to w, of shape (D, M)
  - db: Gradient with respect to b, of shape (M,)
  """
  x, w, b = cache
  N = x.shape[0]
  dx = dout.dot(w.T).reshape(x.shape)
  dw = x.reshape(N, -1).T.dot(dout)
  db = dout.sum(axis=0)
  return dx, dw, db


def relu_forward(x):
  """
  Computes the forward pass for a layer of rectified linear units (ReLUs).

  Input:
  - x: Inputs, of any shape

  Returns a tuple of:
  - out: Output, of the same shape as x
  - cache: x
  """
  out = x.clip(min=0)
  cache = x
  return out, cache


def relu_backward(dout, cache):
  """
  Computes the backward pass for a layer of rectified linear units (ReLUs).

  Input:
  - dout: Upstream derivatives, of any shape
  - cache: Input x, of same shape as dout

  Returns:
  - dx: Gradient with respect to x
  """
  x = cache
  dx = dout.copy()
  dx[x < 0] = 0
  return dx


def conv_forward_naive(x, w, b, conv_param):
  """
  A naive implementation of the forward pass for a convolutional layer.

  The input consists of N data points, each with C channels, height H and width
  W. We convolve each input with F different filters, where each filter spans
  all C channels and has height HH and width WW.

  Input:
  - x: Input data of shape (N, C, H, W)
  - w: Filter weights of shape (F, C, HH, WW)
  - b: Biases, of shape (F,)
  - conv_param: A dictionary with the following keys:
    - 'stride': The number of pixels between adjacent receptive fields in the
      horizontal and vertical directions.
    - 'pad': The number of pixels that will be used to zero-pad the input.

  Returns a tuple of:
  - out: Output data, of shape (N, F, H', W') where H' and W' are given by
    H' = 1 + (H + 2 * pad - HH) / stride
    W' = 1 + (W + 2 * pad - WW) / stride
  - cache: (x_padded, w, b, conv_param)
  """
  N, _, H, W  = x.shape
  F, _, HH, WW = w.shape
  S = conv_param['stride']
  P = conv_param['pad']
  Hp = 1 + (H + 2*P - HH)/S
  Wp = 1 + (W + 2*P - WW)/S
  no_pad, pad = (0, 0), (P, P)
  out = np.zeros((N, F, Hp, Wp))
  for in_act, out_act in itertools.izip(x, out):
    in_padded = np.pad(in_act, [(0, 0), (P, P), (P, P)], mode='constant', constant_values=0)
    for out_act_slice, kernel, bias in itertools.izip(out_act, w, b):
      for j, k in itertools.product(range(Hp), range(Wp)):
        out_act_slice[j, k] = np.sum(in_padded[:, j*S:j*S+HH, k*S:k*S+WW] * kernel) + bias
  cache = (x, w, b, conv_param)
  return out, cache


def conv_backward_naive(dout, cache):
  """
  A naive implementation of the backward pass for a convolutional layer.

  Inputs:
  - dout: Upstream derivatives.
  - cache: A tuple of (x, w, b, conv_param) as in conv_forward_naive

  Returns a tuple of:
  - dx: Gradient with respect to x
  - dw: Gradient with respect to w
  - db: Gradient with respect to b
  """
  x, w, b, conv_param = cache
  _, _, HH, WW = w.shape
  _, _, Hp, Wp = dout.shape
  S = conv_param['stride']
  P = conv_param['pad']
  dw = np.zeros_like(w)
  db = np.zeros_like(b)
  dx = np.zeros_like(x)
  for in_act, din_act, dout_act in itertools.izip(x, dx, dout):
    in_padded = np.pad(in_act, [(0, 0), (P, P), (P, P)], mode='constant', constant_values=0)
    din_padded = np.zeros_like(in_padded)
    for slice_idx, (kernel, dkernel, dout_act_slice) in enumerate(itertools.izip(w, dw, dout_act)):
      for j, k in itertools.product(range(Hp), range(Wp)):
        din_padded[:, j*S:j*S+HH, k*S:k*S+WW] += dout_act_slice[j, k] * kernel
        dkernel += dout_act_slice[j, k] * in_padded[:, j*S:j*S+HH, k*S:k*S+WW]
        db[slice_idx] += dout_act_slice[j, k]
    din_act[:] = din_padded[:, P:-P, P:-P]
  return dx, dw, db


def max_pool_forward_naive(x, pool_param):
  """
  A naive implementation of the forward pass for a max pooling layer.

  Inputs:
  - x: Input data, of shape (N, C, H, W)
  - pool_param: dictionary with the following keys:
    - 'pool_height': The height of each pooling region
    - 'pool_width': The width of each pooling region
    - 'stride': The distance between adjacent pooling regions

  Returns a tuple of:
  - out: Output data
  - cache: (x, pool_param)
  """
  N, C, H, W = x.shape
  S, ph, pw = [pool_param[k] for k in ['stride', 'pool_height', 'pool_width']]
  Hp = 1 + (H - ph)/S
  Wp = 1 + (W - pw)/S
  window_shape = (ph, pw)
  out = np.zeros((N, C, Hp, Wp), dtype=x.dtype)
  dx = np.zeros_like(x)
  for in_act, out_act, din_act in itertools.izip(x, out, dx):
    for i, j in itertools.product(range(Hp), range(Wp)):
      for depth_slice in range(C):
        window_idx = (depth_slice, slice(i*S, i*S+ph), slice(j*S, j*S+pw))
        in_act_window, din_act_window = in_act[window_idx], din_act[window_idx]
        max_ind = np.unravel_index(np.argmax(in_act_window), window_shape)
        out_act[depth_slice, i, j] = in_act_window[max_ind]
        din_act_window[max_ind] = 1
  cache = (dx, x)
#  cache = (x, pool_param)
  return out, cache

def max_pool_backward_naive(dout, cache):
  """
  A naive implementation of the backward pass for a max pooling layer.

  Inputs:
  - dout: Upstream derivatives
  - cache: A tuple of (x, pool_param) as in the forward pass.

  Returns:
  - dx: Gradient with respect to x
  """
  dx, x = cache
  return x


def svm_loss(x, y):
  """
  Computes the loss and gradient using for multiclass SVM classification.

  Inputs:
  - x: Input data, of shape (N, C) where x[i, j] is the score for the jth class
    for the ith input.
  - y: Vector of labels, of shape (N,) where y[i] is the label for x[i] and
    0 <= y[i] < C

  Returns a tuple of:
  - loss: Scalar giving the loss
  - dx: Gradient of the loss with respect to x
  """
  N = x.shape[0]
  correct_class_scores = x[np.arange(N), y]
  margins = np.maximum(0, x - correct_class_scores[:, np.newaxis] + 1.0)
  margins[np.arange(N), y] = 0
  loss = np.sum(margins) / N
  num_pos = np.sum(margins > 0, axis=1)
  dx = np.zeros_like(x)
  dx[margins > 0] = 1
  dx[np.arange(N), y] -= num_pos
  dx /= N
  return loss, dx


def softmax_loss(x, y):
  """
  Computes the loss and gradient for softmax classification.

  Inputs:
  - x: Input data, of shape (N, C) where x[i, j] is the score for the jth class
    for the ith input.
  - y: Vector of labels, of shape (N,) where y[i] is the label for x[i] and
    0 <= y[i] < C

  Returns a tuple of:
  - loss: Scalar giving the loss
  - dx: Gradient of the loss with respect to x
  """
  probs = np.exp(x - np.max(x, axis=1, keepdims=True))
  probs /= np.sum(probs, axis=1, keepdims=True)
  N = x.shape[0]
  loss = -np.sum(np.log(probs[np.arange(N), y])) / N
  dx = probs.copy()
  dx[np.arange(N), y] -= 1
  dx /= N
  return loss, dx

